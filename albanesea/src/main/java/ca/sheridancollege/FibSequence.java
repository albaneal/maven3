package ca.sheridancollege;

public class FibSequence {

	// variation 1
	/*
	 * public static int fib(int n) { return 0; }
	 */

	// variation 2
	/*
	 * public static int fib(int n) { if (n == 0) return 0; return 1; }
	 */

	// variation 3
	/*
	 * public static int fib(int n) { if (n == 0) return 0; if (n <= 2) return 1;
	 * return 2; }
	 */

	// variation 4
	/*
	 * public static int fib(int n) { if (n == 0) return 0; if (n <= 2) return 1;
	 * return 1 + 1; }
	 */

	// variation 5
	/*
	 * public static int fib(int n) { if (n == 0) return 0; if (n <= 2) return 1;
	 * return fib(n - 1) + 1; }
	 */

	// variation 6
	/*
	 * public static int fib(int n) { if (n == 0) return 0; if (n <= 2) return 1;
	 * return fib(n - 1) + fib(n - 2); }
	 */

	// variation 7
	public static int fib(int n) {
		if (n == 0)
			return 0;
		if (n == 1)
			return 1;
		return fib(n - 1) + fib(n - 2);
	}
}
