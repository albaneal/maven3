package ca.sheridancollege.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import ca.sheridancollege.FibSequence;

public class FibSequenceTest {

		@Test
		public void testFibonacci1() {
		 assertEquals(0, FibSequence.fib(0));
		 }

		@Test
		public void testFibonacci2() {
		 assertEquals(0, FibSequence.fib(0));
		 assertEquals(1, FibSequence.fib(1));
		 }

		@Test
		public void testFibonacci3() {
		 int cases[][]= {{0,0},{1,1}};
		 
		 for (int i= 0; i < cases.length; i++)
		 assertEquals(cases[i][1], FibSequence.fib(cases[i][0]));
		 }

		@Test
		public void testFibonacci4() {

		 int cases[][]= {{0,0},{1,1},{2,1}};

		 for (int i= 0; i < cases.length; i++)
		 assertEquals(cases[i][1], FibSequence.fib(cases[i][0]));
		 }

		@Test
		public void testFibonacci5() {
		 int cases[][]= {{0,0},{1,1},{2,1},{3,2}};
		 for (int i= 0; i < cases.length; i++)
		 assertEquals(cases[i][1], FibSequence.fib(cases[i][0]));
		 }
}
